
import itertools
import serial
import time
import sys

from CobsonFrame import CobsonFrame

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def main(args):
    try:
        # Prepare COM port
        comport = serial.Serial('COM14')
        comport.baudrate = int(1e6)
        comport.timeout  = 0.5
        comport.parity   = serial.PARITY_NONE
        comport.stopbits = int(2)
        comport.bytesize = serial.EIGHTBITS
        
        if comport.isOpen():
            print(comport)
        else:
            print("COM opening port error")

        # Init test frame
        tx_frame = CobsonFrame()
        tx_frame.addr = b'\x00'
        tx_frame.data = b'\x00'
        # Now, select ECHO command
        tx_frame.cmd = b'\xFD'
        
        # And start echo test
        start_time = time.time()
        for i in range(1, 8):
            test_data = bytearray(incrementing_payload(i))
            tx_frame.data = test_data

            comport.write(tx_frame.get_raw_frame())

            rx_array = bytearray()
            data = comport.read()
            while data != b'\x00':
                rx_array.extend(data)
                data = comport.read()
                if (data == b''):
                    break

            print(rx_array)

            if (len(rx_array) > 0):
                rx_frame = CobsonFrame(rx_array)

                print("TX:", tx_frame)
                print("RX:", rx_frame)

                assert tx_frame == rx_frame

                # if (cnt_frame % 1000 == 0):
                #     print("Tested", cnt_frame, "out of", n_iterations)
            else:
                print("Unsuccessful reading")
        elapsed_time = float(time.time() - start_time)
        # print("Average FPS is:", (cnt_frame/elapsed_time))
    except Exception as e:
        print("Error", e)

if __name__ == '__main__':
    main(sys.argv)
